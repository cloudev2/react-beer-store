import axios from 'axios';

import { response } from './mockResponse';
import { baseUrl, key } from './constant';

const instance = axios.create({
  baseURL: baseUrl,
  params: key,
  timeout: 1000,
  headers: { 'Content-type': 'application/json' },
});

export const getBeers = async () => {
  // const response = await instance.get(`beers?key=${key}`);
  // const response = jsonResponse;
  console.log(response.data);
  return response.data;
};
