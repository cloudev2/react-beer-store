import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { GlobalState } from '../../store/configureStore';
import { Container } from './BeerList.style';
import BeerDetail from '../Beer/Beer';
import { beerRequest } from '../../store/Beer/action';
import { BeerTypes } from '../../store/Beer/types';

const BeerList = () => {
  const dispatch = useDispatch();
  const beers = useSelector((state: GlobalState) => state.beerState.beers);

  React.useEffect(() => {
    dispatch(beerRequest());
    //beerRequest();
  }, []);

  return (
    <Container>
      {beers.map((beer, index: number) => (
        <BeerDetail beer={beer} key={index} />
      ))}
    </Container>
  );
};

export default BeerList;
