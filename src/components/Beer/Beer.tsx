import * as React from 'react';
import { BeerCard, Button } from './Beer.style';
import { Beer, Labels } from '../../store/Beer/types';
import { useDispatch, useSelector } from 'react-redux';
import { addCartItem } from '../../store/Cart/action';
import { GlobalState } from '../../store/configureStore';

interface BeerProps {
  beer: Beer;
}

const BeerDetail = ({ beer }: BeerProps) => {
  const dispatch = useDispatch();
  const items = useSelector((state: GlobalState) => state.cartState.cart);

  const addCart = (beer: Beer) => {
    const beerItem = items.filter((elm) => elm.beer.id === beer.id)[0];
    dispatch(addCartItem({ beer: beer, quantity: (beerItem?.quantity ?? 0) + 1 }));
  };

  const renderLabels = (beer: Beer) => (
    <img className="image" src={beer.labels?.medium ?? beer.labels?.icon ?? ''} alt={`${beer.name} - logo`} />
  );

  const renderFallbackLabel = () => (
    <img
      className="image"
      src="https://image.shutterstock.com/image-photo/chilled-bottle-dark-beer-260nw-105701495.jpg"
      alt={`${beer.name} - logo`}
    />
  );

  return (
    <BeerCard>
      <h2>{beer.nameDisplay}</h2>
      {beer.labels ? renderLabels(beer) : renderFallbackLabel()}
      <div className="description">
        <p>{beer.style?.shortName ?? beer.style?.name ?? 'No Name.'}</p>
      </div>
      <div className="action">
        <Button
          type="button"
          onClick={() => {
            addCart(beer);
          }}
        >
          Buy
        </Button>
      </div>
    </BeerCard>
  );
};

export default BeerDetail;
