import styled from 'styled-components';

export const BeerCard = styled.div`
  width: 15rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 1rem;
  padding: 1rem;
  border: 1px solid black;
  border-radius: 15px;

  .description {
    text-align: center;
  }
`;

export const Button = styled.button`
  background: black;
  color: white;
  border: 0;
  width: 6rem;
  height: 2rem;
`;
