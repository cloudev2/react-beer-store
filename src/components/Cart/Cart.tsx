import * as React from 'react';
import { GlobalState } from '../../store/configureStore';
import { useSelector, useDispatch } from 'react-redux';
import { ListItem, CartContainer, Container } from './Cart.style';
import { useState } from 'react';
import { addCartItem, removeCartItem } from '../../store/Cart/action';
import { Beer } from '../../store/Beer/types';
import { CartStateType, CartState } from '../../store/Cart/types';

const Cart = () => {
  const dispatch = useDispatch();
  const items = useSelector((state: GlobalState) => state.cartState);

  const changeQuantity = (ev: React.ChangeEvent<HTMLInputElement>, element: CartStateType) => {
    if (element.quantity < +ev.target.value) {
      dispatch(
        addCartItem({
          beer: element.beer,
          quantity: +ev.target.value,
        }),
      );
    } else {
      dispatch(
        removeCartItem({
          beer: element.beer,
          quantity: +ev.target.value,
        }),
      );
    }
  };

  const renderItem = (items: CartState) => {
    return (
      <CartContainer>
        {items.cart.map((beerElement, idx) => (
          <ListItem key={idx}>
            <img
              src={
                beerElement.beer?.labels?.icon ??
                'https://image.shutterstock.com/image-photo/chilled-bottle-dark-beer-260nw-105701495.jpg'
              }
            />
            <p>{beerElement.beer.style?.name ?? beerElement.beer.style?.shortName ?? 'No Name.'}</p>
            <div className="description">
              <p>unit price: ${beerElement.beer.abv}</p>

              <input
                type="number"
                value={beerElement.quantity}
                onChange={(ev) => {
                  changeQuantity(ev, beerElement);
                }}
              />
            </div>
          </ListItem>
        ))}
        <h2>Total: ${items.total.toFixed(2)}</h2>
      </CartContainer>
    );
  };

  return <Container>{items && items.cart.length > 0 ? renderItem(items) : <></>}</Container>;
};

export default Cart;
