import styled from 'styled-components';

export const Container = styled.div`
  position: fixed;
  h2 {
    font-size: 3rem;
  }
`;

export const CartContainer = styled.div`
  max-height: 90vh;
  border: 1px solid black;
  overflow: auto;
`;

export const ListItem = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  margin-bottom: 3rem;
  margin-top: 4rem;
  font-size: 2rem;
  text-align: center;
  padding-left: 2rem;
  padding-right: 2rem;

  .description {
    display: flex;
    width: 100%;
    justify-content: space-evenly;
    font-size: 3rem;
    margin-top: 2rem;
    border-bottom: 2px solid;
    padding-bottom: 2rem;
  }

  input {
    width: 5rem;
  }
`;
