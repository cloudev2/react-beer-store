import { EmptyAction, PayloadAction, action } from 'typesafe-actions';
import { Beer } from '../Beer/types';
import { CartType, CartStateType } from './types';

export const addCartItem = (item: CartStateType): PayloadAction<CartType.CART_ADD, CartStateType> =>
  action(CartType.CART_ADD, item);
export const removeCartItem = (item: CartStateType): PayloadAction<CartType.CART_REMOVE, CartStateType> =>
  action(CartType.CART_REMOVE, item);
