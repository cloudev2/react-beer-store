import { Reducer } from 'redux';
import { CartState, CART_INITIAL_STATE, CartType, CartStateType } from './types';
import { Beer } from '../Beer/types';

type CartReducerType = {
  type: CartType;
  payload: CartStateType;
};

const cartReducer: Reducer<CartState> = (state: CartState = CART_INITIAL_STATE, action: CartReducerType) => {
  switch (action.type) {
    case CartType.CART_ADD:
      const payload: CartStateType = action.payload;

      //calculate the price
      const elementIndex = state.cart.findIndex((elm) => elm.beer.id === payload.beer.id);
      const newItem = state.cart ?? [];

      //const amount: number = payload.quantity * (+payload.beer?.abv ?? 1);

      if (elementIndex >= 0) {
        //payload.quantity += newItem[elementIndex].quantity;
        newItem.splice(elementIndex, 1, payload);
      } else {
        newItem.push({
          beer: payload.beer,
          quantity: 1,
        });
      }

      const amount: number = newItem.map((elm) => elm.quantity * +elm.beer.abv).reduce((accum, curr) => accum + curr);

      /**
       * API does not provide beer price, this way, I decide to use abv, pretending be the price
       * */
      return { ...state, cart: [...newItem], total: amount };
    case CartType.CART_REMOVE:
      const cartElement: CartStateType = action.payload;
      //const totalAmount: number = state.total - cartElement.quantity * (+cartElement.beer?.abv ?? 1);
      const beerIndex = state.cart.findIndex((elm) => elm.beer.id === cartElement.beer.id);

      const beerList = state.cart;
      if (beerList[beerIndex].quantity - 1 > 0) {
        beerList[beerIndex].quantity = cartElement.quantity;
      } else {
        beerList.splice(beerIndex, 1);
      }

      const totalAmount: number =
        beerList.length > 0
          ? beerList.map((elm) => elm.quantity * +elm.beer.abv).reduce((accum, curr) => accum + curr)
          : 0;

      return { ...state, cart: [...beerList], total: totalAmount };
    default:
      return { ...state };
  }
};

export default cartReducer;
