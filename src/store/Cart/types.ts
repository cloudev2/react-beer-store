import { Beer } from '../Beer/types';

export enum CartType {
  CART_ADD = '@@cart/CART_ADD',
  CART_REMOVE = '@@cart/CART_REMOVE',
}

export interface CartState {
  cart: CartStateType[];
  total: number;
}

export type CartStateType = {
  beer: Beer;
  quantity: number;
};

export const CART_INITIAL_STATE: CartState = {
  cart: [],
  total: 0,
};
