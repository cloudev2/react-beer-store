import { takeLatest, all, ForkEffect, AllEffect } from 'redux-saga/effects';
import { BeerTypes } from './Beer/types';
import { loadBeers } from './Beer/sagas';

export default function* watchSagas(): Generator<AllEffect<object>> {
  return yield all([takeLatest(BeerTypes.BEER_REQUEST, loadBeers)]);
}
