import { call, put } from 'redux-saga/effects';
import { getBeers } from '../../services/api';
import { beerSuccess, beerFailure } from './action';
import { BeerTypes, Beer } from './types';

export type BeerSagas = {
  type: BeerTypes;
  payload: Beer[];
};

export function* loadBeers(action: BeerSagas): object {
  try {
    const beers = yield call(getBeers);
    yield put(beerSuccess(beers));
  } catch (err) {
    yield put(beerFailure(err));
  }
}
