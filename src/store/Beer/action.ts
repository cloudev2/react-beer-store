import { EmptyAction, PayloadAction, action } from 'typesafe-actions';
import { Beer, BeerTypes } from './types';

export const beerRequest = (): EmptyAction<BeerTypes.BEER_REQUEST> => action(BeerTypes.BEER_REQUEST);
export const beerSuccess = (beerList: Beer[]): PayloadAction<BeerTypes.BEER_SUCCESS, Beer[]> =>
  action(BeerTypes.BEER_SUCCESS, beerList);

export const beerFailure = (error: string): PayloadAction<BeerTypes.BEER_FAILURE, string> =>
  action(BeerTypes.BEER_FAILURE, error);
