import { BeerState, BEER_INITIAL_STATE, BeerTypes, Beer } from './types';
import { Reducer } from 'redux';

type BeerReducer = {
  type: BeerTypes;
  payload: Beer[] | string;
};

const beerReducer: Reducer<BeerState> = (state: BeerState = BEER_INITIAL_STATE, action: BeerReducer) => {
  switch (action.type) {
    case BeerTypes.BEER_SUCCESS:
      return { ...state, beers: action.payload as Beer[], failure: null, request: false };
    case BeerTypes.BEER_FAILURE:
      return { ...state, failure: action.payload.toString(), request: false };
    case BeerTypes.BEER_REQUEST:
      return { ...state, request: true };
    default:
      return { ...state };
  }
};
export default beerReducer;
