import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import watchSagas from './configureSaga';

import { BeerState } from './Beer/types';
import { CartState } from './Cart/types';

import beerReducer from './Beer/reducer';
import cartReducer from './Cart/reducer';

export interface GlobalState {
  beerState: BeerState;
  cartState: CartState;
}

const globalState = combineReducers<GlobalState>({
  beerState: beerReducer,
  cartState: cartReducer,
});

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = composeWithDevTools({});
const store = createStore(globalState, composeEnhancers(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(watchSagas);

export default store;
