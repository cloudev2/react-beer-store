import * as React from 'react';

import BeerList from '../components/BeerList/BeerList';
import Cart from '../components/Cart/Cart';
import { Container } from './index.style';
const IndexPage = () => {
  return (
    <>
      <h1>Welcome to Beer Store </h1>
      <Container>
        <div className="main-section">
          <BeerList />
        </div>
        <div className="cart-section">
          <Cart />
        </div>
      </Container>
    </>
  );
};

export default IndexPage;
