import styled from 'styled-components';

export const Container = styled.div`
  width: 90%;
  margin: auto;
  display: flex;
  flex-direction: row;

  .main-section {
    width: 80%;
  }
  .cart-section {
    width: 20%;
  }
`;
