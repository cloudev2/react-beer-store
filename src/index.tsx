import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import './style.css';
import { store } from './store';

import IndexPage from './pages';

const Index = () => {
  return (
    <Provider store={store}>
      <IndexPage />
    </Provider>
  );
};

ReactDOM.render(<Index />, document.getElementById('root'));
