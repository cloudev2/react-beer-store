# Project Overview

This project is following stack requirements requested previously.
in docs folder is possible to get a result screenshot.

- Was used functional components and styled components.

- In webpack was used only the basics loaders to avoid install to much things and configured only a dev server to run the application.

- using react-redux hooks instead to use mapDispatchToProps and mapStateToProps.

- Looking the React Profiler the result was reasonable for the main goal, for that reason I decide to not use memoized components.

## How to run

After clone the project, install all project dependencies running:

```sh
npm install
```

In case that everything is installed, run the project using:

```sh
npm run dev
```

## Dificulties

- **The api that I was intending to use, does not provide CORS to do a browser request**: I decided to use a mock constant and simulate this request in saga file, this decision was based in our conversation where was said that I shouldn't care about backend services.

- **Problem to run spyon to mock input parameters**: I would need more time to fix this issue, but I left the test incomplete just to evaluate some knowledge in unit test.
